import React, { PropTypes } from 'react';
import styled from './section.styl';

/**
 * Button component
 * @param {object} props receive children elements, button type and style
 */
function Section(props) {
	const section = <section className={`${styled[props.styled]} ${styled[props.option]}`}> {props.children} </section>;

	return (
		section
	);
}

Section.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.array
		,	PropTypes.element]).isRequired
	,
};

/**
 * Serction style
 *
 * description { option } contains some class - full, centered, etc
 */
Section.defaultProps = {
	styled: 'section'
	, option: 'full'
	,
};

export default Section;
