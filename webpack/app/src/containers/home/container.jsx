import React, { Component } from 'react';
// import Button from '../../components/Button';
import Section from '../../components/Section';
import Tabs, { Panel } from '../../components/Tabs';
import Button from '../../components/Button';
import styled from '../../styles/components/input.styl';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: 1
			,
		};
	}

	render() {
		return (
			<Section styled="home" option="centered">
				<Tabs selected={0}>

					<Panel label="Sign in">
						<div>
							<div className={styled['form-group']}>
								<label htmlFor="email" className={styled.label}>E-mail</label>
								<input type="text" name="email" id="email" className={styled.input} />
							</div>

							<div className={styled['form-group']}>
								<label htmlFor="password" className={styled.label}>Password</label>
								<input type="password" name="password" id="password" className={styled.input} />
							</div>
							<Button styled="primary">Sign in</Button>
						</div>
					</Panel>

					<Panel label="Sign up">
						<h1> Sign up </h1>
					</Panel>

				</Tabs>
			</Section>
		);
	}

}

export default Home;
