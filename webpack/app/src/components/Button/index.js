import React, { PropTypes } from 'react';
import styled from './button.styl';

/**
 * Button component
 * @param {object} props receive children elements, button type and style
 */
function Button(props) {
	const button = <button type={props.type} disabled={false} className={styled[props.styled]} onClick={props.onClick}>{props.children}</button>;

	return (
		button
	);
}

Button.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.node
		,	PropTypes.array
		,	PropTypes.element]).isRequired
	,
};

/**
 * Button type and style
 *
 * description { styled } contains some class - default, primary, warning, info, danger
 */
Button.defaultProps = {
	type: 'button'
	, styled: 'default'
	,
};

export default Button;
