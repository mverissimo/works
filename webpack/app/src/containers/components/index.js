import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '../../components/Button';
import Section from '../../components/Section';

import * as userActions from '../../actions/user/';


class Components extends Component {
	// Don't nedd construct, just a test
	constructor(props) { //eslint-disable-line
		super(props);
	}

	handleAddUser = (e) => {
		e.preventDefault();
		const {
			userActions //eslint-disable-line
			,
		} = this.props;

		userActions.addUser('Matheus Verissimo');
	}

	render() {
		return (
			<Section styled="centered">
				<Button styled="info" onClick={this.handleAddUser}>addUser</Button>
			</Section>
		);
	}

}

const mapStateToProps = (state) => {
	return {
		users: state.users
		,
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		userActions: bindActionCreators(userActions, dispatch)
		,
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Components);
