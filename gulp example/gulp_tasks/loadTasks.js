export { default as browserSyncTask } from './browserSync';
export { default as browserifyTask } from './browserify';
export { default as pugTask } from './pug';
export { default as stylusTask } from './stylesheet';
export { default as javscriptTask } from './javascript';
export { default as copyTask } from './copy';
export { default as watchTask } from './watch';
