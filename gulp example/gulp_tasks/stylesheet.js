import gulp from 'gulp';
import stylus from 'gulp-stylus';
import autoprefixer from 'gulp-autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import rucksack from 'gulp-rucksack';
import plumber from 'gulp-plumber';
import rename from 'gulp-rename';
import gulpif from 'gulp-if';

const stylesheetTask = (directories, args) => {
	gulp.task('style:main', () => {
		const isProduction = args.production;

		return gulp.src(directories.stylesheet.in)
			.pipe(plumber())
			.pipe(sourcemaps.init())
			.pipe(stylus({
				compress: isProduction
				,
			}))
			.pipe(rucksack())
			.pipe(autoprefixer({
				browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
				,
			}))
			.pipe(gulpif(isProduction, rename('application.min.css')))
			.pipe(sourcemaps.write(directories.stylesheet.maps))
			.pipe(gulp.dest(directories.stylesheet.out)
			,
			);
	});

	gulp.task('stylesheet', ['style:main']);
};

export default stylesheetTask;
