import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import styled from './tabs.styl';

class Tabs extends Component {
	constructor(props) { //eslint-disable-line
		super(props);

		this.state = {
			selected: this.props.selected
			,
		};
	}

	static propTypes= {
		selected: PropTypes.number
		,	children: PropTypes.oneOfType([
			PropTypes.array
			,	PropTypes.element]).isRequired
		,
	};

	static defaultProps() {
		return {
			selected: 0
			,
		};
	}

	handleClick = (index, e) => {
		e.stopPropagation();
		e.preventDefault();

		this.setState({
			selected: index
			,
		});
	}

	renderLabel() {
		const labels = (child, index) => {
			const activeClass = classNames({
				[styled.item]: true
				,	[styled.active]: this.state.selected === index
				,
			});

			return (
				<li	key={index}	className={activeClass}>
					<a
						href={`#${index}`}
						onClick={ //eslint-disable-line
							this.handleClick.bind(this, index)
						}
					>
						{child.props.label}
					</a>

				</li>
			);
		};

		return (
			<nav className={styled.navigation}>
				<ul className={styled.menu}>
					{
						this.props.children.map(labels)
					}
				</ul>
			</nav>
		);
	}

	renderPanel() {
		return (
			this.props.children[this.state.selected]
		);
	}

	render() {
		return (
			<div className={styled.tab}>
				{ this.renderLabel() }
				{ this.renderPanel() }
			</div>
		);
	}
}

export function Panel(props) {
	return (
		<div className={styled.panel}>
			{props.children}
		</div>
	);
}

Panel.propTypes = {
	label: PropTypes.string.isRequired //eslint-disable-line
	,	children: PropTypes.element.isRequired
	,
};

export default Tabs;
