import React, { PropTypes } from 'react';

function Frame(props) {
	return (
		<div>
			{ props.children }
		</div>
	);
}

Frame.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.array
		,	PropTypes.element]).isRequired
	,
};

export default Frame;

