import gulp from 'gulp';
import browserSync from 'browser-sync';

const browserSyncTask = (directories) => {
	gulp.task('server', () => {
		// Contains html/css/js
		const files = directories.browserSync.watch;

		return browserSync.init(files, {
			open: 'external'
			,	server: {
				baseDir: directories.browserSync.dir
				,
			}
			,
		});
	});

	gulp.task('browserSync', ['server']);
};

export default browserSyncTask;
