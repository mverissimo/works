const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');

const HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
	template: 'static/index.pug'
	,	filename: 'index.html'
	,	filetype: 'html'
	,
});
const HTMLWebpackPugConfig = new HtmlWebpackPugPlugin();

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const cssExtractor = new ExtractTextPlugin('application.css');

const postStylus = require('poststylus');

module.exports = {
	context: path.resolve(__dirname, 'app')
	,	devServer: {
		clientLogLevel: 'none'
		// quiet: true,
		,	noInfo: true
		,	stats: 'errors-only'
		, historyApiFallback: true
		,
	}
	,	entry: {
		html: './static/index.pug'
		,	index: './src/index.jsx'
		,
	}
	,	devtool: 'source-map'
	,	output: {
		path: path.join(__dirname, 'dist')
		,	publicPath: '/'
		,	filename: '[name].bundle.js'
		,
	}
	, module: {
		preLoaders: [{
			test: /\.(js|jsx)$/
			,	exclude: /node_modules/
			, loaders: ['babel', 'eslint-loader']
			,
		}]
		,	loaders: [
			{
				include: /\.pug$/
				,	loaders: ['html-loader', 'pug-html-loader?exports=false']
				,
			}
			,	{
				test: /\.css$/
				,	exclude: /node_modules|bower_components/
				,	loaders: [
					'style?sourceMap'
					, 'css?modules&importLoaders=1&localIdentName=[path]___[name]__[local]___[hash:base64:5]']
				,
			}
			,	{
				test: /\.styl$/
				,	exclude: /node_modules|bower_components/
				,	loaders: ['style', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]', 'stylus']
				,
			}
			,	{
				test: /\.(js|jsx)$/
				, exclude: /node_modules/
				, loader: 'babel'
				, query: {
					presets: ['es2015']
					,
				},
			}
			,	{
				test: /\.(jpe?g|png|gif|svg)$/i
				,	loaders: ['file?hash=sha512&digest=hex&name=[hash].[ext]', 'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false']
				,
			}]
		,
	}
	,	stylus: {
		use: [
			postStylus(['postcss-reporter', 'autoprefixer'])]
			,
	}
	,	resolve: {
		modules: ['app', 'node_modules']
		,	extensions: [
			''
			,	'.js'
			,	'.jsx'
			,	'.html'
			,	'.pug'
			,	'.css'
			,	'.styl']
			,
	}
	,	plugins: [
		HTMLWebpackPluginConfig
		, HTMLWebpackPugConfig
		,	cssExtractor]
		,
};
