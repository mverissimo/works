import gulp from 'gulp';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sourcemaps from 'gulp-sourcemaps';
import plumber from 'gulp-plumber';
import gulpif from 'gulp-if';
import uglify from 'gulp-uglify';

const browserifyTask = (directories, args) => {
	gulp.task('browserify:main', () => {
		const isProduction = args.production;

		return browserify({
			entries: directories.browserify.main
			,	extensions: ['.js', '.jsx']
			,	debug: !isProduction
			,
		})
		.transform(babelify)
		.bundle()
		.pipe(plumber())
		.pipe(source('bundle.js'))
		.pipe(buffer())
		.pipe(sourcemaps.init())
		.pipe(gulpif(!isProduction
			, sourcemaps.write(directories.browserify.maps)))
		.pipe(gulpif(isProduction
			, uglify()))
		.pipe(gulp.dest(directories.browserify.out));
	});

	gulp.task('browserify', ['browserify:main']);
};

export default browserifyTask;
