import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';

import Frame from '../containers/frame';
import Home from '../containers/home';
import Components from '../containers/components';

export function getRoutes() {
	return (
		<Router path="/" component={Frame}>
			<IndexRoute component={Home} />
			<Route path="/components" component={Components}></Route>
		</Router>
	);
}
