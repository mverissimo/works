import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import svgSprite from 'gulp-svg-sprite';
import cache from 'gulp-cache';

const copyTask = (directories) => {
	/**
	 *	Copy images
	 *	Minify images
	 */
	gulp.task('copy:images', () => {
		return gulp.src(directories.image.in)
			.pipe(cache(imagemin({
				optimizationLevel: 3
				,	progressive: true
				,	interlaced: true
				,
			})))

		.pipe(gulp.dest(directories.image.out));
	});

	/**
	 *	Copy svg
	 * 	Genarate spritesheet of svg files
	 */
	gulp.task('copy:svg', () => {
		const options = {
			mode: {
				symbol: {
					render: {
						css: true
						,	scss: false
						,
					}
					,	dest: 'icons'
					,	sprite: 'icons.svg'
					,
				}
				,
			}
			,	shape: {
				id: {
					separator: '-'
					,
				}
				,
			}
			,
		};

		return gulp.src(directories.svg.in)
			.pipe(svgSprite(options))
			.pipe(gulp.dest(directories.svg.out));
	});

	/**
	 *	Copy json
	 *	Move json files to out
	 */
	gulp.task('copy:json', () => {
		return gulp.src(directories.json.in)
			.pipe(gulp.dest(directories.json.out));
	});

	/**
	 *	Copy fonts
	 *  Move fonts file to out
	 */
	gulp.task('copy:fonts', () => {
		return gulp.src(directories.fonts.in)
			.pipe(gulp.dest(directories.fonts.out));
	});

	/**
	 *  Clear
	 *  Clear cache files
	 */
	gulp.task('clear', (done) => {
		const clear = cache.clearAll(done);
		return clear;
	});

	gulp.task('copy', ['copy:images', 'copy:svg', 'copy:json', 'copy:fonts']);
};

export default copyTask;
