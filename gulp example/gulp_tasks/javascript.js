import gulp from 'gulp';
import jshint from 'gulp-jshint';

const javascriptTask = (directories) => {
	gulp.task('script:main', () => {
	return gulp.src(directories.javascript.in)
			.pipe(jshint())
			.pipe(jshint.reporter('default'))
			.pipe(gulp.dest(directories.javascript.out));
	});

	gulp.task('javascript', ['script:main']);
};

export default javascriptTask;
