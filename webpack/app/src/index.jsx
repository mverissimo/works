import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { getRoutes } from './routes/route';
import configureStore from './store/configure-store';
import './styles/variables.styl';
import './styles/global.styl';

// import { addUser } from './actions/user/';

// Render on server
// const initialState = document.getElementById('initialState');
// const store = configureStore(initialState ? JSON.parse(initialState.text) : {});

const store = configureStore();
// store.dispatch(addUser('Matheus Verissimo'));

render(
	<Provider store={store}>
		<Router routes={getRoutes(store)} history={browserHistory} />
	</Provider>
	,	document.getElementById('app')
	,
);
