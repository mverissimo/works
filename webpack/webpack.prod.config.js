const path = require('path');
const Webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');

const HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
	template: 'static/templates/pages/index.pug'
	,	filename: 'index.html'
	,	filetype: 'html'
	,
});

const HTMLWebpackPugConfig = new HtmlWebpackPugPlugin();

const DefinePlugin = new Webpack.DefinePlugin({
	'process.env': {
		NODE_ENV: JSON.stringify('production'),
	}
	,
});

const UglifyPlugin = new Webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } });
const DedupePlugin = new Webpack.optimize.DedupePlugin();
const CommonChunksPlugin = new Webpack.optimize.CommonsChunkPlugin({ names: ['vendor', 'manifest'] });

module.exports = {
	context: path.resolve(__dirname, 'app')
	,	entry: {
		html: './static/templates/index.pug'
		,	index: './src/index.js'
		,
	}
	, output: {
		path: path.join(__dirname, 'dist')
		,	filename: '[name].[chunkhash].js'
		,	chunkFilename: '[chunkhash].bundle.js'
		,
	}
	, module: {
		loaders: [
			{
				include: /\.pug$/
				,	loader: 'pug-html-loader?pretty'
				,
			}
			,	{
				test: /\.js$/
				, exclude: /node_modules/
				, loader: 'babel'
				, query: {
					presets: ['es2015']
					,
				}
				,
			}
			,	{
				test: /\.(jpe?g|png|gif|svg)$/i
				,	loaders: ['file?hash=sha512&digest=hex&name=[hash].[ext]', 'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false']
				,
			}]
			,
	}
	,	plugins: [
		DefinePlugin
		, HTMLWebpackPluginConfig
		, HTMLWebpackPugConfig
		, UglifyPlugin
		, DedupePlugin
		, CommonChunksPlugin]
		,
};
