import * as types from '../../constants/user';
import { removeProps } from '../../utils/';

const initialState = {
	users: [1, 2]
	, usersById: {
		1: {
			id: 1
			, name: 'Matheus',
		}
		, 2: {
			id: 2
			, name: 'Verissimo',
		},
	},
};

function users(state = initialState, action) {
	switch (action.type) {

		case types.ADD_USER: {
			const newId = state.users[state.users.length - 1] + 1;
			return {
				users: state.users.concat(newId)
				, usersById: {
					...state.usersById
					, [newId]: {
						id: newId
						, name: action.name
						,
					},
				},
			};
		}

		case types.DELETE_USER: {
			return {
				...state
				,	users: state.users.filter((id) => id !== action.id)
				,	usersById: removeProps(state.usersById, action.id)
				,
			};
		}
		default: {
			return state;
		}
	}
}

export default users;
