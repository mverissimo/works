import gulp from 'gulp';
import pug from 'gulp-pug';
import plumber from 'gulp-plumber';

const pugTask = (directories, args) => {
	gulp.task('pug:main', () => {
		const isProduction = args.production;

		return gulp.src(directories.html.in)
			.pipe(plumber())
			.pipe(pug({
				pretty: !isProduction
				,
			}))
			.pipe(gulp.dest(directories.html.out));
	});

	gulp.task('pug', ['pug:main']);
};

export default pugTask;
