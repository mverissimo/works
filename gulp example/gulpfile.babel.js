import gulp from 'gulp';
import argv from 'yargs';
import directories from './config/gulp-directories.json';
import * as gulpModules from './gulp_tasks/loadTasks';

const args = argv.default('production', false)
	.alias('prod', 'production')
	.argv;

/*
 * Optional you can use this
 * https://gist.github.com/mverissimo/ee9a5dbead5d67b9431bf34ec7974621#file-gulpfile-js
 */
for (let key in gulpModules) {
	gulpModules[key](directories, args);
}

gulp.task('default', ['browserSync', 'watch', 'pug', 'stylesheet', 'javascript', 'copy', 'browserify']);
