import gulp from 'gulp';
import gulpif from 'gulp-if';
import cachebust from 'gulp-cache-bust';

const cachebustTask = (args) => {
	gulp.task('cachebust', () => {
		const isProduction = args.production;

		return gulp.src('./dist/index.html')
			.pipe(gulpif(isProduction, cachebust()))
			.pipe(gulp.dest('./dist'));
	});
};

export default cachebustTask;
