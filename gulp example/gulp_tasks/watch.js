import gulp from 'gulp';

const watchTask = (directories) => {
	const files = {
		pug: directories.html.files
		, javascript: directories.javascript.in
		, stylesheet: directories.stylesheet.files
		,
	};

	gulp.task('watch', () => {
		gulp.watch(files.pug
			, ['pug']);

		gulp.watch(files.stylesheet
			, ['stylesheet']);

		gulp.watch(files.javascript, ['javascript'
			, 'browserify']);
	});
};

export default watchTask;
